## 0.10.1
* Updated ocelot-brain to version 0.20.0
* [Ocelot Desktop][]: Updated FAQ

## 0.10.0
* Added changelog button to landing page
* FAQ: Added new components according to last version of ocelot
* Updated ocelot-brain to version 0.18.2 (corresponds to OpenComputers 1.8.3)
* [Ocelot Desktop][]: Combined `release` and `dev` info to a single `desktop/info` API endpoint
* [Ocelot Desktop][]: Updated FAQ

## 0.9.0
* [Ocelot Desktop][]: Fixed release build synchronization
* [Ocelot Desktop][]: Added new API endpoint for getting releases: `/desktop/release`

## 0.8.1
* [Ocelot Desktop][]: Added holographic projector component support into FAQ page

## 0.8.0
* [Ocelot Desktop][]: Now main page suggests download release version by default, instead of development build
* [Ocelot Desktop][]: Added margins to FAQ.html page to improve readability on wide displays
* [Ocelot Desktop][]: Fixed table size overflow in FAQ.html page on small (tablet-size) displays

## 0.7.1
* Updated ocelot-brain to version 0.14.3 (corresponds to OpenComputers 1.8.1, also bugfixes)
* [Ocelot Desktop][]: Updated the FAQ page (note about `ExceptionInInitializerError` on Linux)

## 0.7.0
* Updated ocelot-brain to version 0.14.0 (now corresponds to OpenComputers 1.8.0)
* [Ocelot Desktop][]: Allowed to turn off GitLab info synchronization

## 0.6.2
* [Ocelot Desktop][]: Updated the FAQ page

## 0.6.1
* [Ocelot Desktop][]: Updated the FAQ page

## 0.6.0
* Updated ocelot-brain to version 0.10.0
* [Ocelot Desktop][]: Updated FAQ page
* [Ocelot Desktop][]: Added download mirror
* [Ocelot Desktop][]: Added a new endpoint to check for Ocelot Desktop updates 

## 0.5.3
* [Ocelot Desktop][]: Updated FAQ page

## 0.5.2
* Reset EEPROM if the emulated computer crashed on start

## 0.5.1
* [Ocelot Desktop][]: Refined site contents
* [Ocelot Desktop][]: Layout optimizations on the FAQ page for mobile devices

## 0.5.0
* [Ocelot Desktop][]: Added video background
* [Ocelot Desktop][]: Added SVG logo animation
* [Ocelot Desktop][]: Changed fonts
* [Ocelot Desktop][]: Slightly changed FAQ page

## 0.4.1
* Added a link to Ocelot Desktop page from the landing
* Updated the dependencies & Ocelot Brain version

## 0.4.0
* Added: [Ocelot Desktop][] frontpage

## 0.3.11
* Fix: allowed real numbers in mouse wheel event delta

## 0.3.10
* Security fix: official patch for the "too long without yielding" issue

## 0.3.9
* Fix: `computer.getDeviceInfo()`

## 0.3.8
* Updated to 0.4.0 version of ocelot-brain (workspaces & persistence)

## 0.3.7
* Security patch (resource exhaustion mitigation)

## 0.3.6
* Updated to 0.3.1 version of ocelot-brain (OC upgraded to 1.7.4)

## 0.3.5
* Updated to 0.2.7 version of ocelot-brain (now hard drives are persisted)

## 0.3.4
* Added on more hard drive (this time in the unmanaged mode)

## 0.3.3
* Added randomly generated user names
* Reconfigured test instance to prohibit computer owning
* Added `computer.beep(frequency, duration)` support for client-side

## 0.3.2
* Refactored font rendering (again)
* Fixed: inverted scroll event
* Fixed: vertical gpu.set()

## 0.3.1
* Added active WS connections counter (people online)
* Fixed some rendering glitches in Firefox / Linux

## 0.3.0
* Finished pixel perfect rendering
* Added redstone card Tier 2
* Renamed OpenOS floppy
* Added `hpm` to default OpenOS image
* Fixed copy-pasting

## 0.2.2
* Experimental rendering changes
* Experimental support for clipboard pasting with `Ctrl + V`
* Experimental support for mobile phones
* Added character codes for `Enter`, `Tab`, `Backspace` and `Ctrl + C`
* Fix: release all pressed keys when the page loses focus

## 0.2.1
* Added changelog
* Changed default Lua BIOS to advancedLoader (thanks Luca_S)

## 0.2.0
* Fixed render bugs
* Improved CSS styles
* Added `turn on` and `turn off` buttons
* Added support for resolution changes
* Mouse events support
* Added one more memory plank Tier 3.5
* Added HDD Tier 3
* New redstone card component (thanks Laine)
* Footer with [ocelot.online] version

## 0.1.0
* Basic version.  
One workspace with Tier 4 computer case, Tier 3 CPU, one memory plank Tier 3.5,
internet card, graphics card Tier 3 and screen Tier 2. OpenOS Lua BIOS and OpenOS
floppy included.

[Ocelot Desktop]: https://gitlab.com/cc-ru/ocelot/ocelot-desktop
