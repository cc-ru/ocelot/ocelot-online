package totoro.ocelot.online.api

import spray.json.{DefaultJsonProtocol, RootJsonFormat}

case class GitlabRelease(tag_name: String, released_at: String, assets: Assets)
case class Assets(links: Seq[Links])
case class Links(direct_asset_url: String)

object GitlabRelease extends DefaultJsonProtocol {
  implicit val linksFormat: RootJsonFormat[Links] = jsonFormat1(Links)
  implicit val assetsFormat: RootJsonFormat[Assets] = jsonFormat1(Assets)
  implicit val gitlabReleaseFormat: RootJsonFormat[GitlabRelease] = jsonFormat3(GitlabRelease.apply)
}
