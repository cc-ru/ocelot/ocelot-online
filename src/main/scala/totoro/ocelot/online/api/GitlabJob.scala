package totoro.ocelot.online.api

import spray.json.{DefaultJsonProtocol, RootJsonFormat}

case class GitlabJob(commit: Commit)
case class Commit(id: String, web_url: String, authored_date: String, author_name: String, short_id: String)

object GitlabJob extends DefaultJsonProtocol {
  implicit val commitFormat: RootJsonFormat[Commit] = jsonFormat5(Commit)
  implicit val gitlabJobFormat: RootJsonFormat[GitlabJob] = jsonFormat1(GitlabJob.apply)
}
