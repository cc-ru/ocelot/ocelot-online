package totoro.ocelot.online

import java.io.File

import scala.jdk.CollectionConverters._
import com.typesafe.config.{Config, ConfigFactory}

import scala.collection.mutable

class Settings(val config: Config) {
  val serverHost: String = config.getString("server.host")
  val serverPort: Int = config.getInt("server.port")
  val blacklist: mutable.Buffer[String] = config.getStringList("server.blacklist").asScala
  val savePath: String = config.getString("server.save_path")

  val desktopSyncEnabled: Boolean = config.getBoolean("desktop.sync_enabled")
  val desktopGitlabToken: String = config.getString("desktop.gitlab_token")
  val desktopSyncToken: String = config.getString("desktop.sync_token")
  val desktopSyncTokenHeader: String = config.getString("desktop.sync_token_header")

  val clientHost: String = config.getString("client.host")
}

object Settings {
  private var settings: Settings = _

  def get: Settings = settings

  def load(file: File): Unit = {
    settings = new Settings(ConfigFactory.parseFile(file))
  }
}
